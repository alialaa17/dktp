import {
  ADD_ANIMAL,
  REMOVE_ANIMAL,
  GAME_START,
  GAME_END,
  INCREMENT_SCORE,
  DECREMENT_SCORE,
  CLEAR_GAME,
  INCREMENT_TIMER,
  MISS_SHOT,
  SHOW_GAMEOVER,
  ANIMAL_APPEARED,
  ANIMAL_KILLED,
  ANIMAL_ESCAPED,
  PANDA_SAVED,
  UPDATE_STATS
} from "./types";
import {
  getRandomAnimal,
  generateRandomPosition,
  INTERSTITIAL_AD_ID,
  randomFromArray
} from "../components/utils";

let animal_interval = null;
let timer = null;

export const addAnimal = (life = 3000, extra_pandas = 0) => (
  dispatch,
  getState
) => {
  if (!getState().game.playing) return;
  dispatch({
    type: ADD_ANIMAL,
    key: Date.now() + Math.random() * 10,
    animal: {
      species: getRandomAnimal(extra_pandas),
      life,
      position: generateRandomPosition()
    }
  });
};

export const removeAnimal = key => {
  return {
    type: REMOVE_ANIMAL,
    key
  };
};

export const incrementScore = value => {
  return {
    type: INCREMENT_SCORE,
    value
  };
};

export const decrementScore = value => {
  return {
    type: DECREMENT_SCORE,
    value
  };
};

export const missShot = () => {
  return {
    type: MISS_SHOT
  };
};

export const clearGame = () => dispatch => {
  clearInterval(animal_interval);
  clearInterval(timer);
  dispatch({ type: GAME_END });
  dispatch({ type: CLEAR_GAME });
};

export const startGame = () => dispatch => {
  dispatch(clearGame());
  timer = setInterval(() => dispatch(incrementTimer()), 1000);
  dispatch({ type: GAME_START });
};

/*###################### SPEEDS ##############################*/
const slowStart = dispatch => {
  dispatch(addAnimal(randomFromArray([1000, 2000, 3000])));
};
const moderateSpeed = dispatch => {
  for (let i = 0; i < randomFromArray([1, 2]); i++) {
    setTimeout(() => {
      dispatch(addAnimal(randomFromArray([1000, 1200, 1500])));
    }, i * randomFromArray([300 / i, 600 / i, 800 / i]));
  }
};
const coolDown = dispatch => {
  dispatch(addAnimal(randomFromArray([500, 2000]), 20));
};
const fastSpeed = dispatch => {
  for (let i = 0; i < randomFromArray([1, 2, 3]); i++) {
    setTimeout(() => {
      dispatch(
        addAnimal(
          randomFromArray([1000, 1200, 1500, 2000]),
          randomFromArray([1, 2])
        )
      );
    }, i * 300);
  }
};
const fasterSpeed = dispatch => {
  for (let i = 0; i < randomFromArray([2, 3]); i++) {
    setTimeout(() => {
      dispatch(
        addAnimal(
          randomFromArray([1000, 800, 600, 1500, 2000]),
          randomFromArray([2, 1])
        )
      );
    }, i * 400);
  }
};
const flashingPandas = dispatch => {
  if (Math.random() > 0.5) {
    dispatch(addAnimal(randomFromArray([800, 1000, 1200]), 5));
  }
};
/*#####################################################################*/
let randomSpeed = null;
export const incrementTimer = () => (dispatch, getState) => {
  if (!getState().game.playing) return;
  const current_time = getState().game.timer;
  if (current_time >= 0 && current_time < 5) {
    slowStart(dispatch);
  }
  if (current_time >= 5 && current_time < 25) {
    if (!(current_time % 5)) {
      randomSpeed = randomFromArray([
        moderateSpeed.bind(null, dispatch),
        moderateSpeed.bind(null, dispatch),
        coolDown.bind(null, dispatch),
        fastSpeed.bind(null, dispatch)
      ]);
    }
    randomSpeed();
  }
  if (current_time >= 25) {
    if (!(current_time % 5)) {
      randomSpeed = randomFromArray([
        flashingPandas.bind(null, dispatch),
        fastSpeed.bind(null, dispatch),
        fastSpeed.bind(null, dispatch),
        fasterSpeed.bind(null, dispatch)
      ]);
    }
    randomSpeed();
  }
  dispatch({ type: INCREMENT_TIMER });
};

export const gameOver = () => (dispatch, getState) => {
  clearInterval(animal_interval);
  clearInterval(timer);
  dispatch({ type: GAME_END });
  setTimeout(() => {
    dispatch({ type: SHOW_GAMEOVER });
    dispatch({ type: UPDATE_STATS, game: getState().game });
    // if(!getState().game.playing) {
    //     FacebookAds.InterstitialAdManager.showAd(INTERSTITIAL_AD_ID)
    //     .then(didClick => {console.log(didClick)})
    //     .catch(error => {console.log(error)})
    // }
  }, 1000);
};

export const animalKilled = species => {
  return {
    type: ANIMAL_KILLED,
    species
  };
};

export const animalAppeared = species => {
  return {
    type: ANIMAL_APPEARED,
    species
  };
};

export const animalEscaped = species => {
  return {
    type: ANIMAL_ESCAPED,
    species
  };
};

export const pandaSaved = species => {
  return {
    type: PANDA_SAVED
  };
};
