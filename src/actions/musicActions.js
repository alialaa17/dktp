import { UPDATE_MUSIC_OBJECT, UPDATE_MUSIC_STATUS } from './types';

export function loadMusic() {
    return async (dispatch, getState) => {
        const soundObject = getState().music.file;
        
        try {
            await soundObject.loadAsync(require('../../assets/sounds/music.mp3'));
            await soundObject.setIsLoopingAsync(true);
            await soundObject.playAsync();
            const status = await soundObject.getStatusAsync();
            dispatch({type: UPDATE_MUSIC_OBJECT, sound: soundObject});
            dispatch({type: UPDATE_MUSIC_STATUS, status});
        } catch (error) {
            console.log('error at loadMusic action: ', error);
        }
    }
}

export function playMusic() {
    return async (dispatch, getState) => {
        const soundObject = getState().music.file;
        try {
            await soundObject.playAsync();
            const status = await soundObject.getStatusAsync();
            dispatch({type: UPDATE_MUSIC_OBJECT, sound: soundObject});
            dispatch({type: UPDATE_MUSIC_STATUS, status});
        } catch (error) {
            console.log('error at playMusic action: ', error);
        }
    }
}

export function stopMusic() {
    return async (dispatch, getState) => {
        const soundObject = getState().music.file;
        try {
            await soundObject.stopAsync();
            const status = await soundObject.getStatusAsync();
            dispatch({type: UPDATE_MUSIC_OBJECT, sound: soundObject});
            dispatch({type: UPDATE_MUSIC_STATUS, status});
        } catch (error) {
            console.log('error at stopMusic action: ', error);
        }
    }
}

export function pauseMusic() {
    return async (dispatch, getState) => {
        const soundObject = getState().music.file;
        try {
            await soundObject.pauseAsync();
            const status = await soundObject.getStatusAsync();
            dispatch({type: UPDATE_MUSIC_OBJECT, sound: soundObject});
            dispatch({type: UPDATE_MUSIC_STATUS, status});
        } catch (error) {
            console.log('error at pauseMusic action: ', error);
        }
    }
}

export function setMusicVolume(value) {
    return async (dispatch, getState) => {
        const soundObject = getState().music.file;
        try {
            await soundObject.setVolumeAsync(value);
            const status = await soundObject.getStatusAsync();
            dispatch({type: UPDATE_MUSIC_OBJECT, sound: soundObject});
            dispatch({type: UPDATE_MUSIC_STATUS, status});
        } catch (error) {
            console.log('error at setMusicVolume action: ', error);
        }
    }
}