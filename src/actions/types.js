/***********
Music Actions
***********/

export const UPDATE_MUSIC_OBJECT = "UPDATE_MUSIC_OBJECT";
export const UPDATE_MUSIC_STATUS = "UPDATE_MUSIC_STATUS";


/***********
Game Actions
***********/

export const ADD_ANIMAL = "ADD_ANIMAL";
export const REMOVE_ANIMAL = "REMOVE_ANIMAL";
export const GAME_START = "GAME_START";
export const GAME_END = "GAME_END";
export const CLEAR_GAME = "CLEAR_GAME";
export const MISS_SHOT = "MISS_SHOT";
export const SHOW_GAMEOVER = "SHOW_GAMEOVER";
export const INCREMENT_SCORE = "INCREMENT_SCORE";
export const DECREMENT_SCORE = "DECREMENT_SCORE";
export const INCREMENT_TIMER = "INCREMENT_TIMER";


export const ANIMAL_APPEARED = "ANIMAL_APPEARED";
export const ANIMAL_KILLED = "ANIMAL_KILLED";
export const ANIMAL_ESCAPED = "ANIMAL_ESCAPED";
export const PANDA_SAVED = "PANDA_SAVED";


export const UPDATE_SETTINGS = "UPDATE_SETTINGS";
export const UPDATE_STATS = "UPDATE_STATS";
export const RESET_STATS = "RESET_STATS";