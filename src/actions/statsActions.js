import { UPDATE_STATS, RESET_STATS } from './types';

export const updateStats = (game) => {
    return {
        type: UPDATE_STATS,
        game
    }
}

export const resetStats = () => {
    return {
        type: RESET_STATS
    }
}