import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStackNavigator } from 'react-navigation';
import MenuScreen from '../components/MenuScreen';
import GameScreen from '../components/GameScreen';
import TutorialScreen from '../components/TutorialScreen';
import StatsScreen from '../components/StatsScreen';

const AppNavigator = createStackNavigator({
  Main: { screen: MenuScreen },
  Game: { screen: GameScreen },
  Tutorial: { screen: TutorialScreen },
  Stats: { screen: StatsScreen },
}, {
    headerMode: 'none',
    cardStyle: {
        shadowOpacity: 0,
    }
});

export default AppNavigator;