import { UPDATE_MUSIC_OBJECT, UPDATE_MUSIC_STATUS } from '../actions/types';
import { Audio } from 'expo';

export default function music(state = {file: new Audio.Sound(), status: {}}, action){
	switch(action.type){
		case UPDATE_MUSIC_OBJECT:
			return {...state, file: action.sound}
			break;
		case UPDATE_MUSIC_STATUS:
			return {...state, status: action.status}
			break;
		default:
			return state;
	}
}