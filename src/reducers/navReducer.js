import { NavigationActions } from 'react-navigation';

import { AppNavigator } from '../navigators/AppNavigator';

const mainAction = AppNavigator.router.getActionForPathAndParams('Main');
const mainActionState = AppNavigator.router.getStateForAction(mainAction);

export default function nav(state = mainActionState, action) {
  let nextState;
  switch (action.type) {
    case 'Main':
        nextState = AppNavigator.router.getStateForAction(
            NavigationActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'Main' })],
            }),
            state
        );
    break;
    case 'Game':
        nextState = AppNavigator.router.getStateForAction(
            NavigationActions.navigate({ routeName: 'Game' }),
            state
        );
    break;
    case 'Tutorial':
        nextState = AppNavigator.router.getStateForAction(
            NavigationActions.navigate({ routeName: 'Tutorial' }),
            state
        );
    break;
    case 'Stats':
        nextState = AppNavigator.router.getStateForAction(
            NavigationActions.navigate({ routeName: 'Stats' }),
            state
        );
    break;
    default:
        nextState = AppNavigator.router.getStateForAction(action, state);
    break;
  }

  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
}