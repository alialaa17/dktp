import { ADD_ANIMAL, REMOVE_ANIMAL, GAME_START, GAME_END, INCREMENT_SCORE, DECREMENT_SCORE, CLEAR_GAME, INCREMENT_TIMER, MISS_SHOT, SHOW_GAMEOVER, ANIMAL_APPEARED, ANIMAL_KILLED, ANIMAL_ESCAPED, PANDA_SAVED } from '../actions/types';
import { MAX_MISSES } from '../components/utils';

let default_state = {
    animals: {},
    playing: false,
    score: 0,
    timer: 0,
    remaining_misses: MAX_MISSES,
    gameover_modal: false,
    stats: {
        appeared: 0,
        appeared_breakdown: {},
        killed: 0,
        killed_breakdown: {},
        escaped: 0,
        escaped_breakdown: {},
        pandas_saved: 0
    }
};

export default function game(state = default_state, action){
	switch(action.type){
        case ADD_ANIMAL: {
            let animals = Object.assign({}, state.animals);
            animals[action.key] = action.animal;
            return {...state, animals};
            break;
        }
		case REMOVE_ANIMAL: {
            let animals = Object.assign({}, state.animals);
            delete animals[action.key];
            return {...state, animals};
            break;
        }
        case GAME_START: {
            return {...state, playing: true};
            break;
        }
        case GAME_END: {
            return {...state, playing: false};
            break;
        }
        case SHOW_GAMEOVER: {
            return {...state, gameover_modal: true};
            break;
        }
        case CLEAR_GAME: {
            return {...default_state};
            break;
        }
        case INCREMENT_SCORE: {
            return {...state, score: state.score + action.value};
            break;
        }
        case DECREMENT_SCORE: {
            return {...state, score: state.score - action.value};
            break;
        }
        case INCREMENT_TIMER: {
            return {...state, timer: state.timer + 1};
            break;
        }
        case MISS_SHOT: {
            return {...state, remaining_misses: state.remaining_misses - 1};
            break;
        }
        case ANIMAL_KILLED: {
            let killed_breakdown = Object.assign({}, state.stats.killed_breakdown);
            killed_breakdown[action.species] = (action.species in killed_breakdown) ? killed_breakdown[action.species] + 1 : 1;
            return {...state, stats: { ...state.stats, killed: state.stats.killed + 1, killed_breakdown} };
            break;
        }
        case ANIMAL_APPEARED: {
            let appeared_breakdown = Object.assign({}, state.stats.appeared_breakdown);
            appeared_breakdown[action.species] = (action.species in appeared_breakdown) ? appeared_breakdown[action.species] + 1 : 1;
            return {...state, stats: { ...state.stats, appeared: state.stats.appeared + 1, appeared_breakdown} };
            break;
        }
        case ANIMAL_ESCAPED: {
            let escaped_breakdown = Object.assign({}, state.stats.escaped_breakdown);
            escaped_breakdown[action.species] = (action.species in escaped_breakdown) ? escaped_breakdown[action.species] + 1 : 1;
            return {...state, stats: { ...state.stats, escaped: state.stats.escaped + 1, escaped_breakdown} };
            break;
        }
        case PANDA_SAVED: {
            return {...state, stats: { ...state.stats, pandas_saved: state.stats.pandas_saved + 1} };
            break;
        }
		default:
			return state;
	}
}