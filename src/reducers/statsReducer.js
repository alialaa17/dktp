import { UPDATE_STATS, RESET_STATS } from '../actions/types';
import { MAX_MISSES } from '../components/utils';

const default_state = {
	games_playes: 0,
	highest_score: 0,
	total_score: 0,
	total_time: 0,
	longest_game: 0,
	highest_score_time: 0,
	longest_game_score: 0,
	animals_appeared: 0,
	animals_killed: 0,
	pandas_killed: 0,
	pandas_saved: 0,
	animals_missed: 0,
	bullets_shot: 0,
	bullets_wasted: 0
};

export default function stats(state = default_state, action){
	switch(action.type){
		case RESET_STATS: {	
			return {...state, ...default_state}
			break;
		}
		case UPDATE_STATS: {
			let game = action.game;
			let games_playes = state.games_playes + 1;
			let highest_score = (game.score > state.highest_score) ? game.score : state.highest_score;
			let total_score = state.total_score + game.score;
			let total_time = state.total_time + game.timer;
			let longest_game = (game.timer > state.longest_game) ? game.timer : state.longest_game;

			let highest_score_time = state.highest_score_time;
			if(game.score > state.highest_score) {
				highest_score_time = game.timer;
			} else if(game.score === state.highest_score) {
				highest_score_time = (game.timer < state.highest_score_time) ? game.timer : state.highest_score_time;
			}

			let longest_game_score = state.longest_game_score;
			if(game.timer > state.longest_game) {
				longest_game_score = game.score;
			} else if(game.timer === state.longest_game) {
				longest_game_score = (game.score > state.longest_game_score) ? game.score : state.longest_game_score;
			}

			let animals_appeared = state.animals_appeared + game.stats.appeared;
			let animals_killed = state.animals_killed + game.stats.killed;
			let animals_missed = state.animals_missed + game.stats.escaped;
			let pandas_saved = state.pandas_saved + game.stats.pandas_saved;
			let pandas_killed = state.pandas_killed + (game.stats.killed_breakdown['panda'] ? game.stats.killed_breakdown['panda'] : 0);

			let bullets_shot = state.bullets_shot + (game.stats.killed + MAX_MISSES - game.remaining_misses);
			let bullets_wasted = state.bullets_wasted + (MAX_MISSES - game.remaining_misses);

			return {...state, games_playes, highest_score, total_score, total_time, longest_game, highest_score_time, longest_game_score, animals_appeared, animals_killed, animals_missed, pandas_saved, pandas_killed, bullets_shot, bullets_wasted}
			break;
		}
		default:
			return state;
	}
}