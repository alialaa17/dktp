import { UPDATE_SETTINGS } from '../actions/types';

const default_state = {
	opened: false,
    music: true,
    sounds: true,
    gun: 0
};

export default function settings(state = default_state, action){
	switch(action.type){
		case UPDATE_SETTINGS:		
			return {...state, ...action.settings}
			break;
		default:
			return state;
	}
}