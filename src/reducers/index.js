import { combineReducers } from 'redux';
import music from './musicReducer';
import game from './gameReducer';
import settings from './settingsReducer';
import stats from './statsReducer';

const AppReducer = combineReducers({
  music,
  game,
  settings,
  stats
});
export default AppReducer;