import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Text, View, Image } from 'react-native';
import { timeToText, MAX_MISSES, IMAGES, slugToName, GRID_AREA_WIDTH, FONT_FAMILY } from './utils';
import MenuButton from './MenuButton';

const text_shadow= { 
    textShadowColor: '#000',
    textShadowOffset: {width: 1, height: 1},
    textShadowRadius: 0 
};

const styles = StyleSheet.create({
    list_wrap: {
        flexWrap: 'wrap', 
        alignItems: 'flex-start', 
        flexDirection:'row', 
        marginHorizontal: -5
    },
    list_item: {
        flexDirection: 'row', 
        alignItems: 'center', 
        width: (GRID_AREA_WIDTH - 30)/8, 
        paddingHorizontal: 5, 
        marginBottom: 5
    },
    times:{
        color: '#fff', 
        fontFamily: FONT_FAMILY
    },
    name: {
        color: '#fff', 
        fontFamily: FONT_FAMILY, 
        opacity: 0.8
    }, 
    text: {
        color: '#fff', 
        fontFamily: FONT_FAMILY, 
        fontSize: 19, 
    },
    score_title: {
        color: '#fff', 
        fontFamily: FONT_FAMILY, 
        fontSize: 22,
        textShadowColor: '#000',
        textShadowOffset: {width: 1, height: 1},
        textShadowRadius: 0
    },
    score_text: {
        color: '#fff', 
        fontFamily: FONT_FAMILY, 
        fontSize: 72,
        textShadowColor: '#000',
        textShadowOffset: {width: 3, height: 3},
        textShadowRadius: 0
    }
});

class GameScreen extends React.Component {

    render() {
        let { score, timer, remaining_misses, stats } = this.props.game;
		return (
			<View>

                <View style={{alignItems: 'center', marginTop: 20}}>
                    <Text style={styles.score_title}>You Scored</Text>
                    <Text  style={styles.score_text}>{score}</Text>
                    <Text style={styles.text}><Text style={{opacity: 0.6}}>Time:</Text> {timeToText(timer)}</Text>
                    <Text style={styles.text}><Text style={{opacity: 0.6}}>Bullets Shot:</Text> {stats.killed + MAX_MISSES - remaining_misses}</Text>
                    <Text style={styles.text}><Text style={{opacity: 0.6}}>Bullets Wasted:</Text> {MAX_MISSES - remaining_misses}</Text>
                </View>

                <View style={{marginTop: 20}}>

                    {(Object.keys(stats.killed_breakdown).length > 0) &&
                        <View>
                            <Text style={[styles.text, {fontSize: 22, marginBottom: 10, ...text_shadow}]}>
                                {stats.appeared} animals showed up.
                            </Text>

                            <Text style={[styles.text, {fontSize: 18, marginBottom: 5}]}>
                                {stats.killed} killed in action:
                            </Text>
                            <View style={styles.list_wrap}>
                                {Object.keys(stats.killed_breakdown).map((species) => {
                                    return (
                                        <View key={species} style={styles.list_item}>
                                            <Text style={styles.times}>x{stats.killed_breakdown[species]}</Text>
                                            <Image style={{width: 20, height: 20}} source={IMAGES[species]} />
                                            {/* <Text style={styles.name}> {slugToName(species)} {(species === 'panda') ? '😢' : ''}</Text> */}
                                        </View>
                                    );
                                })}
                            </View>
                        </View>
                    }
                    {(Object.keys(stats.killed_breakdown).length === 0) &&
                        <Text style={[styles.text, {fontSize: 18, marginBottom: 5, marginTop: 10}]}>
                            No animals were killed.
                        </Text>
                    }
                    
                    {(Object.keys(stats.escaped_breakdown).length > 0) &&
                        <View>
                            <Text style={[styles.text, {fontSize: 18, marginBottom: 5, marginTop: 10}]}>
                                {stats.escaped} managed to escape:
                            </Text>
                            <View style={styles.list_wrap}>
                                {Object.keys(stats.escaped_breakdown).map((species) => {
                                    return (
                                        <View key={species} style={styles.list_item}>
                                            <Text style={styles.times}>x{stats.escaped_breakdown[species]}</Text>
                                            <Image style={{width: 20, height: 20}} source={IMAGES[species]} />
                                            {/* <Text style={styles.name}> {slugToName(species)} {(species === 'panda') ? '✌' : ''}</Text> */}
                                        </View>
                                    );
                                })}
                            </View>
                        </View>
                    }
                    {(Object.keys(stats.escaped_breakdown).length === 0) &&
                        <Text style={[styles.text, {fontSize: 18, marginBottom: 5, marginTop: 10}]}>
                            No animals managed to escape.
                        </Text>
                    }

                    {(stats.pandas_saved > 0) &&
                        <Text style={[styles.text, {fontSize: 18, marginBottom: 5, marginTop: 10}]}>
                            {stats.pandas_saved} pandas were saved.
                        </Text>
                    }
                    

                </View>              

            </View>
		);
    }
}

function mapStateToProps(state, props){
    return {
		game: state.game,
	}
}

export default connect(mapStateToProps, { })(GameScreen);
