import React from "react";
import { StyleSheet, Text, View, Image, ScrollView } from "react-native";
import { connect } from "react-redux";
import { LinearGradient, WebBrowser } from "expo";
import {
  loadMusic,
  playMusic,
  pauseMusic,
  setMusicVolume,
  updateSettings
} from "../actions";
import MenuButton from "./MenuButton";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // height: SCREEN_HEIGHT - 20,
    backgroundColor: "transparent"
  },
  button: {
    marginBottom: 10
  },
  small_button: {
    marginBottom: 3
  },
  logo: {
    width: 220,
    height: 185,
    marginTop: 50,
    marginBottom: 100
  }
});

class MenuScreen extends React.Component {
  async componentWillMount() {
    this.props.updateSettings({ opened: false });
    if (this.props.settings.music) {
      if (!this.props.isLoaded) {
        await this.props.loadMusic();
        this.props.playMusic();
      } else {
        this.props.playMusic();
      }
    }
  }

  async toggleMusic() {
    if (this.props.isPlaying) {
      this.props.pauseMusic();
      this.props.updateSettings({ music: false });
    } else {
      if (!this.props.isLoaded) {
        await this.props.loadMusic();
      }
      this.props.playMusic();
      this.props.updateSettings({ music: true });
    }
  }

  toggleSettings() {
    if (this.props.settings.opened) {
      this.props.updateSettings({ opened: false });
    } else {
      this.props.updateSettings({ opened: true });
    }
  }
  toggleSounds() {
    if (this.props.settings.sounds) {
      this.props.updateSettings({ sounds: false });
    } else {
      this.props.updateSettings({ sounds: true });
    }
  }
  toggleGun() {
    if (this.props.settings.gun) {
      this.props.updateSettings({ gun: 0 });
    } else {
      this.props.updateSettings({ gun: 1 });
    }
  }

  render() {
    return (
      <LinearGradient colors={["#c5b0ff", "#008ed4"]} style={styles.container}>
        <ScrollView>
          <View style={{ alignItems: "center" }}>
            <Image
              resizeMode="contain"
              style={styles.logo}
              source={require("../../assets/images/logo.png")}
            />

            <View style={{ paddingHorizontal: 20, minWidth: 300 }}>
              <MenuButton
                style={styles.button}
                onPress={() => {
                  this.props.navigation.navigate("Game");
                }}
                text="New Game"
              />
              <MenuButton
                style={styles.button}
                onPress={() => {
                  this.props.navigation.navigate("Tutorial");
                }}
                text="How To Play"
              />
              <MenuButton
                style={styles.button}
                onPress={() => {
                  this.props.navigation.navigate("Stats");
                }}
                text="Statistics"
              />
              <MenuButton
                style={styles.button}
                onPress={() => {
                  this.toggleSettings();
                }}
                text="Settings"
              />
              {this.props.settings.opened && (
                <View>
                  <MenuButton
                    backgroundColor="#000"
                    borderColor="#eee"
                    size="small"
                    style={styles.small_button}
                    onPress={() => {
                      this.toggleMusic();
                    }}
                    text={this.props.isPlaying ? "Music On" : "Music Off"}
                  />
                  <MenuButton
                    backgroundColor="#000"
                    borderColor="#eee"
                    size="small"
                    style={styles.small_button}
                    onPress={() => {
                      this.toggleSounds();
                    }}
                    text={
                      this.props.settings.sounds ? "Sounds On" : "Sounds Off"
                    }
                  />
                  <MenuButton
                    backgroundColor="#000"
                    borderColor="#eee"
                    size="small"
                    style={styles.small_button}
                    onPress={() => {
                      this.toggleGun();
                    }}
                    text={this.props.settings.gun ? "Laser Gun" : "Shot Gun"}
                  />
                </View>
              )}
            </View>

            <View
              style={{ paddingHorizontal: 20, marginTop: 40, marginBottom: 40 }}
            >
              <Text
                style={{
                  color: "rgba(255, 255, 255, 0.5)",
                  fontFamily: "pequena",
                  fontSize: 18
                }}
                onPress={() =>
                  WebBrowser.openBrowserAsync("https://twitter.com/alialaa")
                }
              >
                Created By @alialaa
              </Text>
            </View>
          </View>
        </ScrollView>
        {/* <View style={{top: -20}}>
                    <FacebookAds.BannerView
                        placementId={BANNER_AD_ID}
                        type="standard"
                        onPress={() => console.log('click')}
                        onError={(err) => console.log('error', err)}
                    />
                        </View> */}
      </LinearGradient>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    isPlaying: state.music.status.isPlaying,
    isLoaded: state.music.status.isLoaded,
    settings: state.settings
  };
}

export default connect(
  mapStateToProps,
  { loadMusic, playMusic, pauseMusic, setMusicVolume, updateSettings }
)(MenuScreen);
