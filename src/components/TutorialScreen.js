import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity
} from "react-native";
import MenuButton from "./MenuButton";
import { LinearGradient } from "expo";
import { Ionicons } from "@expo/vector-icons";
import { FONT_FAMILY } from "./utils";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "transparent"
  },
  header: {
    paddingVertical: 15,
    paddingHorizontal: 60
  },
  back_button: {
    width: 60,
    alignItems: "center",
    justifyContent: "center",
    top: 0,
    bottom: 0,
    position: "absolute",
    left: 0
  },
  header_title: {
    color: "#fff",
    fontFamily: FONT_FAMILY,
    fontSize: 26,
    textAlign: "center"
  },
  content: {
    paddingHorizontal: 15,
    paddingVertical: 30
  },
  point: {
    color: "#fff",
    fontSize: 20,
    fontFamily: FONT_FAMILY
  }
});

class TutorialScreen extends React.Component {
  render() {
    return (
      <LinearGradient colors={["#af92e2", "#673ab7"]} style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
            style={styles.back_button}
          >
            <Ionicons name="md-arrow-round-back" size={32} color="#fff" />
          </TouchableOpacity>
          <Text style={styles.header_title}>How to Play</Text>
        </View>

        <ScrollView contentContainerStyle={styles.content}>
          <Text style={styles.point}>
            - Don't kill the pandas 🚫
            <Image
              resizeMode="cover"
              style={{ width: 22, height: 22 }}
              source={require("../../assets/images/panda.png")}
            />
          </Text>
          <Text style={styles.point}>- Kill a panda -> GameOver 🤕</Text>
          <Text style={styles.point}>
            - Kill another animal{" "}
            <Image
              resizeMode="cover"
              style={{ width: 24, height: 24 }}
              source={require("../../assets/images/wolf.png")}
            />{" "}
            -> 1 point
          </Text>
          <Text style={styles.point}>- Another animal escapes -> -1 point</Text>
          <Text style={styles.point}>- Waste a bullet -> -1 point</Text>
          <Text style={styles.point}>- Waste 5 bullets -> GameOver</Text>

          <MenuButton
            onPress={() => this.props.navigation.navigate("Game")}
            style={{ marginTop: 30 }}
            text="Play Now"
            size="small"
          />
        </ScrollView>
      </LinearGradient>
    );
  }
}

export default TutorialScreen;
