import React from 'react';
import { connect } from 'react-redux';
import { Button, StyleSheet, Text, View, Image, Dimensions, TouchableWithoutFeedback } from 'react-native';
import { removeAnimal, gameOver, incrementScore, decrementScore, animalKilled, animalAppeared, animalEscaped, pandaSaved } from '../actions';
import { IMAGES, ANIMAL_SIZE, GUNS, GROANS, KILL_SCORE, MISS_SCORE, ESCAPE_SCORE } from './utils';
import { Audio } from 'expo';

const styles = StyleSheet.create({

});

class Animal extends React.Component {
    gunSound = new Audio.Sound()
    groanSound = new Audio.Sound()
    popSound = new Audio.Sound()
    animalLife = null
    killed = false
    componentDidMount() {
        if(this.props.sounds) {
            GLOBAL.requestAnimationFrame(async() => {
                try {
                    await this.popSound.loadAsync(require('../../assets/sounds/pop.mp3'));
                    await this.popSound.playAsync();
                    setTimeout(async () => {
                        await this.popSound.unloadAsync();
                        this.popSound.setOnPlaybackStatusUpdate(null);
                        this.popSound = null;
                    }, 500);
                } catch (error) {
                    console.log('Error at Pop Sound', error)
                }
            });
        }
        this.props.animalAppeared(this.props.data.species);
        this.animalLife = setTimeout(() => {
            this.props.removeAnimal(this.props.id)
        }, this.props.data.life)
        // console.log('IN',this.props.data.species, this.killed);
    }

    componentWillUnmount() {
        if(this.props.playing) {
            if(this.props.data.species !== 'panda') {
                this.props.decrementScore(ESCAPE_SCORE);               
            }
            if(!this.killed) {
                if(this.props.data.species !== 'panda') {
                    this.props.animalEscaped(this.props.data.species);
                } else {
                    this.props.pandaSaved();
                }
            }
            // console.log('OUT',this.props.data.species, this.killed);
        }
    }

    componentWillReceiveProps(nextProps) {
        if(!nextProps.playing) {
            clearTimeout(this.animalLife);
        }
    }

    async handleClick() {
        if(!this.props.playing) return;
        this.killed = true;
        this.props.animalKilled(this.props.data.species);
        if(this.props.data.species === 'panda') {
            this.props.gameOver();
            if(this.props.sounds) {
                let pandaGroan = new Audio.Sound();
                try {
                    await this.gunSound.loadAsync(GUNS[this.props.gun]);
                    this.gunSound.playAsync();

                    await pandaGroan.loadAsync(require('../../assets/sounds/angry_2.mp3'));
                    await pandaGroan.playAsync();
                    setTimeout(async () => {
                        await pandaGroan.unloadAsync();
                        pandaGroan.setOnPlaybackStatusUpdate(null);
                        pandaGroan = null;
                    }, 3000);
                } catch (error) {
                    console.log('Error at Panda Groan Sound', error)
                }
            }
            return;
        }
        this.props.onShoot();
        this.props.incrementScore(KILL_SCORE + ESCAPE_SCORE);
        if(this.props.sounds) {
            GLOBAL.requestAnimationFrame(async() => {
                try {
                    await this.gunSound.loadAsync(GUNS[this.props.gun]);
                    await this.gunSound.playAsync();

                    setTimeout(async () => {        
                        await this.gunSound.unloadAsync();  
                        await this.gunSound.setOnPlaybackStatusUpdate(null);  
                        this.gunSound = null;
                    }, 2000);                  

                    await this.groanSound.loadAsync(GROANS[Math.floor(Math.random()*GROANS.length)]);
                    await this.groanSound.setVolumeAsync(0.8);
                    await this.groanSound.playAsync();

                    setTimeout(async () => {
                        await this.groanSound.unloadAsync();
                        await this.groanSound.setOnPlaybackStatusUpdate(null);
                        this.groanSound = null;
                    }, 2000);
                   
                } catch (error) {
                    console.log('Error at Gun Sound', error)
                } 
            });
        }
    }

    render() {
        let { data, style } = this.props;
		return (
            <TouchableWithoutFeedback onPress={() => this.handleClick()}>
                <Image resizeMode="cover" style={[{width: ANIMAL_SIZE, height: ANIMAL_SIZE, position: 'absolute'}, style]} source={IMAGES[data.species]} />
            </TouchableWithoutFeedback>
		);
    }
}

function mapStateToProps(state, props){
    return {
        playing: state.game.playing,
        sounds: state.settings.sounds,
        gun: state.settings.gun
    }
}


export default connect(mapStateToProps, { removeAnimal, gameOver, incrementScore, decrementScore, animalKilled, animalAppeared, animalEscaped, pandaSaved })(Animal);
