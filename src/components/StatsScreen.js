import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, ScrollView, Text, View, Image, TouchableOpacity, Alert } from 'react-native';
import MenuButton from './MenuButton';
import { LinearGradient } from 'expo';
import { Ionicons } from '@expo/vector-icons';
import { FONT_FAMILY, toHHMMSS } from './utils';
import { resetStats } from '../actions';


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'transparent',
    },
    header: {
        paddingVertical: 15, 
        paddingHorizontal: 60
    },
    back_button: {
        width: 60, 
        alignItems: 'center', 
        justifyContent: 'center', 
        top: 0, 
        bottom: 0, 
        position: 'absolute', 
        left: 0
    },
    header_title: {
        color: '#000', 
        fontFamily: FONT_FAMILY, 
        fontSize: 26, 
        textAlign: 'center'
    },
    content: {
        paddingHorizontal: 15,
        paddingVertical: 30
    },
    stats_box: {
        backgroundColor: '#f03', 
        padding: 15, 
        borderRadius: 3, 
        marginBottom: 5
    },
    stats_title: {
        fontFamily: FONT_FAMILY, 
        color: '#fff', 
        textAlign: 'center', 
        fontSize: 18, 
        opacity: 0.8
    },
    stats_value: {
        fontFamily: FONT_FAMILY, 
        color: '#fff', 
        textAlign: 'center', 
        fontSize: 34
    }
});

class StatsScreen extends React.Component {

    handleResetButton() {
        Alert.alert(
            'Delete All Statistics',
            'You are about to reset all the previous statistics. Are yous sure?',
            [
              {text: 'Cancel', style: 'cancel'},
              {text: 'Delete', onPress: () => this.props.resetStats(), style: "destructive"},
            ],
            { cancelable: false }
        )
    }
  
    render() {

        let { games_playes, highest_score, total_score, total_time, longest_game, highest_score_time, longest_game_score, animals_appeared, animals_killed, pandas_killed, pandas_saved, animals_missed, bullets_shot, bullets_wasted } = this.props.stats;

        return (
            <LinearGradient colors={['#fff3ee', '#efd5ca']} style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.back_button}>
                        <Ionicons name="md-arrow-round-back" size={32} color="#000" />
                    </TouchableOpacity>
                    <Text style={styles.header_title}>Statistics</Text>
                </View>

                <ScrollView contentContainerStyle={styles.content}>
                    
                    <View style={{flexDirection: 'row', marginRight: 5}}>
                        <View style={{flex: 1}}>
                            <View style={[styles.stats_box, { backgroundColor: '#f44336'}]}>
                                <Text style={styles.stats_title}>Games Played</Text>
                                <Text style={styles.stats_value}>{games_playes}</Text>
                            </View>
                            <View style={[styles.stats_box, { backgroundColor: '#03a9f4'}]}>
                                <Text style={styles.stats_title}>Highest Score</Text>
                                <Text style={styles.stats_value}>{highest_score}</Text>
                            </View>
                            <View style={[styles.stats_box, { backgroundColor: '#8bc34a'}]}>
                                <Text style={styles.stats_title}>Longest Game</Text>
                                <Text style={styles.stats_value}>{toHHMMSS(longest_game)}</Text>
                            </View>
                            <View style={[styles.stats_box, { backgroundColor: '#ffc107'}]}>
                                <Text style={styles.stats_title}>Highest Score Time</Text>
                                <Text style={styles.stats_value}>{toHHMMSS(highest_score_time)}</Text>
                            </View>
                            <View style={[styles.stats_box, { backgroundColor: '#8e7f79'}]}>
                                <Text style={styles.stats_title}>Animals Killed</Text>
                                <Text style={styles.stats_value}>{animals_killed}</Text>
                            </View>
                            <View style={[styles.stats_box, { backgroundColor: '#222'}]}>
                                <Text style={styles.stats_title}>Pandas Killed</Text>
                                <Text style={styles.stats_value}>{pandas_killed}</Text>
                            </View>
                            <View style={[styles.stats_box, { backgroundColor: '#009688'}]}>
                                <Text style={styles.stats_title}>Bullets Shot</Text>
                                <Text style={styles.stats_value}>{bullets_shot}</Text>
                            </View>
                        </View>
                        <View style={{flex: 1, marginLeft: 5}}>
                            <View style={[styles.stats_box, { backgroundColor: '#9c27b0'}]}>
                                <Text style={styles.stats_title}>Total Score</Text>
                                <Text style={styles.stats_value}>{total_score}</Text>
                            </View>
                            <View style={[styles.stats_box, { backgroundColor: '#e91e63'}]}>
                                <Text style={styles.stats_title}>Total Time</Text>
                                <Text style={styles.stats_value}>{toHHMMSS(total_time)}</Text>
                            </View>
                            <View style={[styles.stats_box, { backgroundColor: '#607d8b'}]}>
                                <Text style={styles.stats_title}>Longest Game Score</Text>
                                <Text style={styles.stats_value}>{longest_game_score}</Text>
                            </View>
                            <View style={[styles.stats_box, { backgroundColor: '#8e7f79'}]}>
                                <Text style={styles.stats_title}>Animals Appeared</Text>
                                <Text style={styles.stats_value}>{animals_appeared}</Text>
                            </View>
                            <View style={[styles.stats_box, { backgroundColor: '#8e7f79'}]}>
                                <Text style={styles.stats_title}>Animals Missed</Text>
                                <Text style={styles.stats_value}>{animals_missed}</Text>
                            </View>
                            <View style={[styles.stats_box, { backgroundColor: '#222'}]}>
                                <Text style={styles.stats_title}>Pandas Saved</Text>
                                <Text style={styles.stats_value}>{pandas_saved}</Text>
                            </View>
                            <View style={[styles.stats_box, { backgroundColor: '#009688'}]}>
                                <Text style={styles.stats_title}>Bullets Wasted</Text>
                                <Text style={styles.stats_value}>{bullets_wasted}</Text>
                            </View>
                        </View>
                    </View>

                    <MenuButton 
                        onPress={() => this.handleResetButton()}
                        style={{marginTop: 30}}
                        text="Reset Statistics" 
                        size="small"
                    />
                </ScrollView>
            </LinearGradient>
        );
    }
}

function mapStateToProps(state, props){
    return {
		stats: state.stats
	}
}

export default connect(mapStateToProps, { resetStats })(StatsScreen);