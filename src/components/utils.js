import { Dimensions, Platform } from 'react-native';

import {
    createReactNavigationReduxMiddleware,
    createReduxBoundAddListener,
  } from 'react-navigation-redux-helpers';
  

export const { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } = Dimensions.get('window');
export const TOP_AREA_HEIGHT = 120;
export const GRID_AREA_WIDTH = SCREEN_WIDTH;
export const GRID_AREA_HEIGHT = SCREEN_HEIGHT - TOP_AREA_HEIGHT;
export const ANIMAL_SIZE = 57;

export const FONT_FAMILY = 'pequena';

export const BANNER_AD_ID = (Platform.OS === 'ios') ? '117352428935439_117533482250667' : '117352428935439_118178095519539';
export const INTERSTITIAL_AD_ID = (Platform.OS === 'ios') ? '117352428935439_117356112268404' : '117352428935439_118178205519528';

export const KILL_SCORE = 1;
export const MISS_SCORE = 1;
export const ESCAPE_SCORE = 1;
export const MAX_MISSES = 5;

export const IMAGES = {
    'brown_bear': require('../../assets/images/brown_bear.png'),
    'cat': require('../../assets/images/cat.png'),
    'elephant': require('../../assets/images/elephant.png'),
    'fox': require('../../assets/images/fox.png'),
    'frog': require('../../assets/images/frog.png'),
    'hamster': require('../../assets/images/hamster.png'),
    'horse': require('../../assets/images/horse.png'),
    'moose': require('../../assets/images/moose.png'),
    'panda': require('../../assets/images/panda.png'),
    'penguin': require('../../assets/images/penguin.png'),
    'pig': require('../../assets/images/pig.png'),
    'polar_bear': require('../../assets/images/polar_bear.png'),
    'rabbit': require('../../assets/images/rabbit.png'),
    'wolf': require('../../assets/images/wolf.png'),
}

export const GUNS = [
    require('../../assets/sounds/shot_gun.mp3'), 
    require('../../assets/sounds/laser_gun.mp3')
]

export const GROANS = [
    require('../../assets/sounds/angry_1.mp3'), 
    require('../../assets/sounds/angry_3.mp3'), 
    require('../../assets/sounds/angry_4.mp3'), 
    require('../../assets/sounds/angry_5.mp3'), 
    require('../../assets/sounds/angry_6.mp3'), 
    require('../../assets/sounds/angry_7.mp3')
]

export const getRandomAnimal = (extra_pandas = 0) => {
    let animals = Object.keys(IMAGES);
    let extra_pandas_array = Array(extra_pandas).fill('panda');
    animals = [...animals, ...extra_pandas_array];
    return animals[Math.floor(Math.random()*animals.length)];
}

export const generateRandomPosition = () => {
    return {
        left: Math.random()*(GRID_AREA_WIDTH - 55), 
        top: Math.random()*(GRID_AREA_HEIGHT - 55)
    }
}

export const toHHMMSS = (seconds) => {
    var hours = Math.floor(seconds / 3600);
    var minutes = Math.floor((seconds - (hours * 3600)) / 60);
    var seconds = seconds - (hours * 3600) - (minutes * 60);

    if (hours < 10) { hours = "0" + hours; }
    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }
    return hours + ':' + minutes + ':' + seconds;
}

export const timeToText = (seconds) => {
    var h = Math.floor(seconds / 3600);
    var m = Math.floor(seconds % 3600 / 60);
    var s = Math.floor(seconds % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
    return hDisplay + mDisplay + sDisplay; 
}

export const slugToName = (species) => {
    return species.replace('_', ' ').replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

export const randomFromArray = (array) => {
    return array[Math.floor(Math.random()*array.length)];
}