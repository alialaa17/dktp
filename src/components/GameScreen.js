import React from 'react';
import { connect } from 'react-redux';
import { Button, StyleSheet, Text, View, ScrollView, Image, Dimensions, TouchableWithoutFeedback, TouchableHighlight, Animated, Modal, Share, TouchableOpacity } from 'react-native';
import Animal from './Animal';
import { stopMusic, startGame, removeAnimal, clearGame, gameOver, decrementScore, missShot } from '../actions';
import { IMAGES, TOP_AREA_HEIGHT, GRID_AREA_HEIGHT, ANIMAL_SIZE, GRID_AREA_WIDTH, getRandomAnimal, generateRandomPosition, toHHMMSS, MISS_SCORE, ESCAPE_SCORE, FONT_FAMILY, MAX_MISSES } from './utils';
import { Audio, BlurView, LinearGradient } from 'expo';
import MenuButton from './MenuButton';
import Statistics from './Statistics';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff3ee',
	},
	score_box: {
		alignItems: 'center',
		justifyContent: 'center', 
		paddingHorizontal: 10, 
		paddingVertical: 5, 
		backgroundColor:'#e41e41', 
		borderRadius: 3, 
		marginHorizontal: 3, 
		minWidth: 70,
	},
	score_box_title: {
		color: '#fff', 
		opacity: 0.8, 
		fontFamily: FONT_FAMILY, 
		fontSize: 14, 
		backgroundColor: 'transparent'
	},
	score_box_value: {
		color: '#fff', 
		fontFamily: FONT_FAMILY, 
		fontSize: 25, 
		backgroundColor: 'transparent'
	},
	upper_area: {
		height: TOP_AREA_HEIGHT, 
		backgroundColor: 'transparent', 
		paddingHorizontal: 5, 
		flexDirection: 'row', 
		flex: 1, 
		marginTop: 20
	},
	upper_area_inner: {
		flexDirection: 'row', 
		flex: 1, 
		height: TOP_AREA_HEIGHT - 40
	},
	main_button: {
		backgroundColor: '#3f51b5',
		marginHorizontal: 0, 
		flex: 1, 
		marginBottom: 3
	},
	main_button_text: {
		color: '#fff', 
		fontFamily: FONT_FAMILY, 
		fontSize: 16, 
		backgroundColor: 'transparent'
	},
	timer_box: {
		backgroundColor: '#607d8b',
		marginHorizontal: 0, 
		flex: 1,
		marginTop: 3
	},
	timer_text: {
		color: '#fff', 
		fontFamily: FONT_FAMILY, 
		fontSize: 16, 
		backgroundColor: 'transparent'
	},
});

class GameScreen extends React.Component {
	static navigationOptions = {
		gesturesEnabled : false,
	}

	constructor(props) {
		super(props);
		this.state = {
			_backgroundAnimated: new Animated.Value(0),
		}
	}

	async componentDidMount() {
		if(this.props.isPlaying) {
			this.props.stopMusic(); 
		}
		this.props.startGame();
	}

	componentWillUnmount() {
		this.props.clearGame();
	}

	async shoot() {
		if(!this.props.playing) return;
		this.props.decrementScore(MISS_SCORE);

		if(this.props.remaining_misses !== 1) {
			this.props.missShot();
		} else {
			this.props.missShot();
			this.props.gameOver();
		}

		Animated.timing(this.state._backgroundAnimated, {
			toValue: 100,
			duration: 150
		}).start(() => {
			Animated.timing(this.state._backgroundAnimated, {
				toValue: 0,
				duration: 150
			}).start();
		});  

		if(this.props.sounds) {
			let buzzSound = new Audio.Sound();
			try {
				await buzzSound.loadAsync(require('../../assets/sounds/buzz.mp3'));
				await buzzSound.playAsync();
				setTimeout(async () => {
					await buzzSound.unloadAsync();
					await buzzSound.setOnPlaybackStatusUpdate(null);
					buzzSound = null;
				}, 1000);
			} catch (error) {
				console.log('Error at Buzz Sound', error)
			}
		}
	}

    render() {
		let animals = this.props.animals;
		let interpolatedColorAnimation = this.state._backgroundAnimated.interpolate({
			inputRange: [0,100],
		  	outputRange: ['#fff3ee', '#e66055']
		});
		return (
			<Animated.View style={[styles.container, {backgroundColor: interpolatedColorAnimation}]}>

				<View style={styles.upper_area}>

					<View style={styles.upper_area_inner}>
						<View style={[styles.score_box, { backgroundColor: '#e91e63' }]}>
							<Text style={styles.score_box_title}>Score</Text>
							<Text style={styles.score_box_value}>{this.props.score}</Text>
						</View>

						<View style={[styles.score_box, { backgroundColor: '#03a9f4' }]}>
							<Text style={styles.score_box_title}>Best</Text>
							<Text style={styles.score_box_value}>{this.props.highest_score}</Text>
						</View>

						<View style={[styles.score_box, { backgroundColor: '#9c27b0' }]}>
							<Text style={styles.score_box_title}>Misses</Text>
							<Text style={styles.score_box_value}>{MAX_MISSES - this.props.remaining_misses}/{MAX_MISSES}</Text>
						</View>

						<View style={{flex: 1, marginHorizontal: 5}}>
							<TouchableOpacity activeOpacity={0.8} onPress={() => this.props.navigation.navigate('Main') } style={[styles.score_box, styles.main_button]}>
								<Text style={styles.main_button_text}>Main Menu</Text>
							</TouchableOpacity>
							<View style={[styles.score_box, styles.timer_box]}>
								<Text style={styles.timer_text}>{toHHMMSS(this.props.timer)}</Text>
							</View>
						</View>
					</View>
					
				</View>

				<TouchableWithoutFeedback onPress={() => this.shoot()}>
					<View style={{height: GRID_AREA_HEIGHT}}>
						{Object.keys(animals).map(animal_key => (
							<Animal style={{...animals[animal_key].position}} id={animal_key} key={animal_key} data={animals[animal_key]} onShoot={() => this.props.removeAnimal(animal_key) }  />
						))}
					</View>	
				</TouchableWithoutFeedback>	

				<Modal animationType="slide" transparent={true} visible={this.props.gameover_modal} >
					<BlurView tint="dark" intensity={95} style={StyleSheet.absoluteFill}>
						<ScrollView contentContainerStyle={{padding: 15}}>
							<Statistics />
							<MenuButton size="small" backgroundColor="#008ed4" borderColor="#fff" style={{marginTop: 40}} onPress={() => {   
								Share.share({
									message: `I scored ${this.props.score} on #DontKillThePandas. Download it Now.`
								},
								{
									dialogTitle: 'Share Don\'t Kill the Pandas'
								})
							}} text="Share" />
							{/* <MenuButton size="small" style={{marginTop: 10}} onPress={() => {   
								this.props.startGame();
							}} text="New Game" /> */}
							<MenuButton size="small" style={{marginTop: 10}} onPress={() => {   
								this.props.clearGame();
								this.props.navigation.navigate('Main');
							}} text="Main Menu" />

						</ScrollView>
					</BlurView>
				</Modal>		
				
			</Animated.View>
		);
    }
}

function mapStateToProps(state, props){
    return {
		isPlaying: state.music.status.isPlaying,
		animals: state.game.animals,
		playing: state.game.playing,
		score: state.game.score,
		gameover_modal: state.game.gameover_modal,
		timer: state.game.timer,
		sounds: state.settings.sounds,
		remaining_misses: state.game.remaining_misses,
		highest_score: state.stats.highest_score
	}
}

export default connect(mapStateToProps, { stopMusic, startGame, removeAnimal, clearGame, gameOver, decrementScore, missShot })(GameScreen);
