import React from 'react';
import { Text, View, TouchableOpacity, Platform, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container: {
        paddingTop: 5, 
        paddingLeft: 5
    },
    inner: {
        backgroundColor: '#fff', 
        borderRadius: 4, 
        paddingTop: 12, 
        paddingLeft: 20, 
        paddingRight: 20, 
        paddingBottom: 18
    },
    text: {
        color: '#000', 
        textAlign: 'center', 
        fontWeight: 'bold', 
        fontSize: 22, 
        fontFamily: 'pequena'
    },
    border: {
        borderColor: '#000', 
        borderWidth: 2, 
        position: 'absolute', 
        top: 0, 
        left: 0, 
        right: 5, 
        bottom: 5, 
        borderRadius: 4
    }
});

class MenuButton extends React.Component {
    
	render(){
        let small_text = {};
        let small_padding = {};
        let backgroundColor = this.props.backgroundColor ? this.props.backgroundColor : '#ffffff';
        let borderColor = this.props.borderColor ? this.props.borderColor : '#000000';
        if(this.props.size == 'small') {
            small_text.fontSize = 18;
            small_padding.paddingTop  = 8;
            small_padding.paddingBottom  = 12;
        }
		return(
            <TouchableOpacity activeOpacity={0.5} style={[ styles.container, this.props.style]} onPress={this.props.onPress}>				
                <View style={[styles.inner, small_padding, {backgroundColor}]}>
                    <Text style={[styles.text, small_text, { color: borderColor}]}>{this.props.text}</Text>
                </View>
                <View style={[styles.border, {borderColor}]}></View>
            </TouchableOpacity>
		)
	}
}

export default MenuButton;
