import React from 'react';
import { StyleSheet, Text, View } from 'react-native';


const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20, 
        marginTop: 0, 
        marginBottom: 40
    },
    title: {
        color: 'rgba(255, 255, 255, 0.3)', 
        fontFamily: 'pequena', 
        fontSize: 14
    },
    name: {
        color: 'rgba(255, 255, 255, 0.5)', 
        fontFamily: 'pequena', 
        fontSize: 16
    }
});


export default Credits = props => {  
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Graphics: <Text style={styles.name} onPress={() => WebBrowser.openBrowserAsync('https://graphicriver.net/user/goblinportal')}>GoblinPortal</Text>, <Text style={styles.name} onPress={() => WebBrowser.openBrowserAsync('https://graphicriver.net/user/mochawalk')}>mochawalk</Text></Text>

            <Text style={styles.title}>SFX: <Text style={styles.name} onPress={() => WebBrowser.openBrowserAsync('https://audiojungle.net/user/lynxstudio')}>lynxstudio</Text>, <Text style={styles.name} onPress={() => WebBrowser.openBrowserAsync('https://graphicriver.net/user/mochawalk')}>mochawalk</Text></Text>
        </View>
    )
}