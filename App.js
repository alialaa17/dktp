import React from "react";
import { StatusBar, View, AsyncStorage } from "react-native";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import ReduxThunk from "redux-thunk";
import { persistStore, autoRehydrate } from "redux-persist";
import AppReducer from "./src/reducers";
import AppNavigator from "./src/navigators/AppNavigator";
import { AppLoading, Font } from "expo";

const store = createStore(
  AppReducer,
  {},
  compose(
    applyMiddleware(ReduxThunk),
    autoRehydrate()
  )
);
persistStore(store, {
  storage: AsyncStorage,
  whitelist: ["settings", "stats"]
});

class App extends React.Component {
  state = {
    appReady: false
  };

  async _cacheResourcesAsync() {
    const assets = [
      require("./assets/sounds/music.mp3"),
      require("./assets/sounds/shot_gun.mp3"),
      require("./assets/sounds/laser_gun.mp3"),
      require("./assets/sounds/angry_1.mp3"),
      require("./assets/sounds/angry_2.mp3"),
      require("./assets/sounds/angry_3.mp3"),
      require("./assets/sounds/angry_4.mp3"),
      require("./assets/sounds/angry_5.mp3"),
      require("./assets/sounds/angry_6.mp3"),
      require("./assets/sounds/angry_7.mp3"),
      require("./assets/images/logo.png"),
      require("./assets/images/brown_bear.png"),
      require("./assets/images/cat.png"),
      require("./assets/images/elephant.png"),
      require("./assets/images/fox.png"),
      require("./assets/images/frog.png"),
      require("./assets/images/hamster.png"),
      require("./assets/images/horse.png"),
      require("./assets/images/moose.png"),
      require("./assets/images/panda.png"),
      require("./assets/images/penguin.png"),
      require("./assets/images/pig.png"),
      require("./assets/images/polar_bear.png"),
      require("./assets/images/rabbit.png"),
      require("./assets/images/wolf.png")
    ];

    // for (let asset of assets) {
    // 	await Asset.fromModule(asset).downloadAsync();
    // }

    await Font.loadAsync({
      pequena: require("./assets/fonts/pequena-webfont.ttf")
    });
  }

  componentWillMount() {
    // FacebookAds.AdSettings.addTestDevice(FacebookAds.AdSettings.currentDeviceHash);
  }

  render() {
    if (!this.state.appReady) {
      return (
        <AppLoading
          startAsync={this._cacheResourcesAsync}
          onFinish={() => this.setState({ appReady: true })}
          onError={console.warn}
        />
      );
    }
    return (
      <Provider store={store}>
        <View style={{ flex: 1 }}>
          <StatusBar hidden={true} />
          <AppNavigator />
        </View>
      </Provider>
    );
  }
}

export default App;
